public class SortingTask3 {
    public static void main(String[] args) {
        String[] firstNames = {"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
        String[] lastNames = {"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};
        System.out.println("Unsorted: ");
        display(firstNames);
        display(lastNames);
        sort(firstNames, lastNames);
        System.out.println("Sorted: ");
        display(firstNames);
        display(lastNames);
    }

    public static void display(String[] arr) {
        for (String element : arr) {
            System.out.print(element + " ");
        }
        System.out.println();
    }

    public static void sort(String[] firstNames, String[] lastNames) {
        for (int i = 1; i < firstNames.length; i++) {
            String currName = firstNames[i];
            String currLastName = lastNames[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && firstNames[prevKey].compareTo(currName) > 0) {
                firstNames[prevKey + 1] = firstNames[prevKey];
                firstNames[prevKey] = currName;
                lastNames[prevKey + 1] = lastNames[prevKey];
                lastNames[prevKey] = currLastName;
                prevKey--;
            }
            if (prevKey >= 0 && firstNames[prevKey].compareTo(currName) == 0 && lastNames[prevKey].compareTo(currLastName) > 0) {
                lastNames[prevKey + 1] = lastNames[prevKey];
                lastNames[prevKey] = currLastName;
            }
        }
    }
}